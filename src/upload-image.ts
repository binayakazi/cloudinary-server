import { Request, Response } from "express";
import { IncomingForm } from "formidable";
import { v2 } from "cloudinary";
import config from "./config";

async function uploadImage(req: Request, res: Response) {
  try {
    v2.config({
      cloud_name: config.cloudName,
      api_key: config.apiKey,
      api_secret: config.apiSecret,
    });

    await new Promise((_, reject) => {
      const form = new IncomingForm();

      form.parse(req, (err, fields, files) => {
        if (err) return reject(err);

        if (files.image) {
          v2.uploader.upload(
            files.image[0].filepath,
            {
              folder: "uploads",
              use_filename: true,
            },
            (error, result) => {
              if (error) {
                return reject(error);
              }

              const imageUrl = result?.secure_url;

              console.log(result);
              res.json({
                message: "Successfully Saved Image in Cloudinary",
                image: imageUrl,
              });
            }
          );
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: "Internal server error", error: err });
  }
}

export default uploadImage;
