import express from "express";
import config from "./config";
import cors from "cors";
import uploadImage from "./upload-image";

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("public"));

app.set("port", config.port || 3000);

app.get("/", (req, res) => {
  res.json({ message: "Ping!" });
});

app.post("/upload", uploadImage);

export default app;
