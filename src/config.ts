import { config as _config } from "dotenv";

_config();

const config = {
  port: loadFromEnv("PORT") || 3000,
  cloudName: loadFromEnv("CLOUD_NAME"),
  apiKey: loadFromEnv("API_KEY"),
  apiSecret: loadFromEnv("API_SECRET"),
};

function loadFromEnv(key: string): string {
  const value = process.env[key];
  if (!value) {
    throw new Error(`Missing environment variable: ${key}`);
  }
  return value;
}

export default config;
